﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public enum TreeType
{
	Weak,
	Strong,
	Bush,
};

[System.Serializable]
public struct tree_data
{
	public string Name;
	public Sprite TreeSprite;
	public TreeType Type;
	public float Cost;
	public float Production;
	public float Creation;
	public float LightObstruction;
	public string AbilityDescription;
	public Action<ForestTree> Ability;
}

[System.Serializable]
public struct tree_sprite
{
	public string Name;
	public Sprite TreeSprite;
};

public class GameData : MonoBehaviour {

	public static Dictionary<string, TreeTablet> TabletPrototypes;

    public static List<Viking> WaveVikings;

	public static GameObject TabletPrefab;
	public static GameObject TreePrefab;
	public static GameObject SquirrelPrefab;
    public static GameObject SaplingPrefab;

    public static int townObjects = 0;
    static int _saplings = 40;
    public static int saplings 
    {
    	get
    	{
    		return _saplings;
    	}
    	set
    	{
    		_saplings = value;
    		RefreshSaplings();
    	}
    }

    public static float MinX = -75, MaxX = 10;
    public static ForestTree FurthestTree;

    public static bool VikingGameStarted = false;
    public static bool GameEnded = false;

	static Dictionary<string, Sprite> Sprites;

	public GameObject TabletPrefabStub;
	public GameObject TreePrefabStub;
	public GameObject SquirrelPrefabStub;
    public GameObject SaplingPrefabStub;

    public TextMesh saplingUINumberStub;
    public static TextMesh saplingUINumber;
    public TextMesh GameEndMeshStub;
    public static TextMesh GameEndMesh;

    public AudioClip acornThrowAudioClip, acornHitAudioClip, vikingDie, townHit, fire;

	public List<tree_sprite> TreeSprites;

	void Start () {
		saplingUINumber = saplingUINumberStub;
		GameEndMesh = GameEndMeshStub;
		TabletPrefab = TabletPrefabStub;
		TreePrefab = TreePrefabStub;
		SquirrelPrefab = SquirrelPrefabStub;
        SaplingPrefab = SaplingPrefabStub;
		saplings = 100;
		MinX = -65;
		MaxX = 10;
		GameEnded = false;
		VikingGameStarted = false;

		InitTreeSprites(TreeSprites);

		InitTabletPrototypes();

        WaveVikings = new List<Viking>();

        RefreshSaplings();

        SpawnTablet("Oak", Vector3.right * UnityEngine.Random.Range(-1.0f, 1.0f) + Vector3.up * UnityEngine.Random.Range(1.0f, 3.5f));
        SpawnTablet("Oak", Vector3.right * UnityEngine.Random.Range(-1.0f, 1.0f) + Vector3.up * UnityEngine.Random.Range(1.0f, 3.5f));
        SpawnTablet("Oak", Vector3.right * UnityEngine.Random.Range(-1.0f, 1.0f) + Vector3.up * UnityEngine.Random.Range(1.0f, 3.5f));
        SpawnTablet("Oak", Vector3.right * UnityEngine.Random.Range(-1.0f, 1.0f) + Vector3.up * UnityEngine.Random.Range(1.0f, 3.5f));
        SpawnTablet("Spruce", Vector3.right * UnityEngine.Random.Range(-1.0f, 1.0f) + Vector3.up * UnityEngine.Random.Range(1.0f, 3.5f));
        SpawnTablet("Spruce", Vector3.right * UnityEngine.Random.Range(-1.0f, 1.0f) + Vector3.up * UnityEngine.Random.Range(1.0f, 3.5f));
        SpawnTablet("Raspberry", Vector3.right * UnityEngine.Random.Range(-1.0f, 1.0f) + Vector3.up * UnityEngine.Random.Range(1.0f, 3.5f));
	}

    public static void RefreshSaplings()
    {
        saplingUINumber.text = saplings + "<quad material=1/>";
    }

    public static void DeleteAllGameObjects()
    {
    	GameObject[] GOs = GameObject.FindObjectsOfType<GameObject>();
    	foreach(GameObject GO in GOs)
    	{
    		if(GO != Camera.main.gameObject)
    		{
    			if(GO != GameEndMesh.gameObject)
    			{
    				Destroy(GO);
    			}
    		}
    	}
    }

    public static void CheckLoseGame()
    {
    	if(GameObject.FindObjectsOfType<Squirrel>().Length <= 0)
    	{
    		if(GameObject.FindObjectsOfType<ForestTree>().Length <= 0)
    		{
    			if(Time.time != 0)
    			{
    				ReloadGame();
    				/*DeleteAllGameObjects();
    				GameEndMesh.text = "You LOSE";
    				GameEndMesh.gameObject.SetActive(true);*/
    			}
    		}
    	}
    }

    public static void ReloadGame()
    {
    	if(!GameEnded)
    	{
    		GameEnded = true;
			Application.LoadLevel(0);
		}
    }

    public static void WinGame()
    {
    	GameEnded = true;
    	DeleteAllGameObjects();
    	GameEndMesh.text = "You WIN";
		GameEndMesh.gameObject.SetActive(true);
    }

    public static void StartVikingGame()
    {
    	if(!VikingGameStarted)
    	{
    		VikingGameStarted = true;
    		GameObject.FindObjectsOfType<VikingTown>()[0].StartWaving();
    	}
    }

    public static void SortTabletsInHand()
    {
    	TreeTablet[] Tablets = TabletPrototypes.Values.Where(Tablet => Tablet.Count > 0).ToArray();

    	float SingleWidth = 2.25f;
    	float Start = -SingleWidth * (Tablets.Length%2==0 ? Tablets.Length/2-0.5f : Tablets.Length/2);

    	for(int i = 0;
    		i < Tablets.Length;
    		++i)
    	{
    		float X = Start + i*SingleWidth;
    		Vector3 Pos = Tablets[i].transform.localPosition;
    		Pos.x = X;
    		Tablets[i].transform.localPosition = Pos;
    	}
    }

	public static void InitTreeSprites(List<tree_sprite> TSs)
	{
		Sprites = new Dictionary<string, Sprite>();

		foreach(tree_sprite TS in TSs)
		{
			Sprites.Add(TS.Name, TS.TreeSprite);
		}
	}
	
	public static void InitTabletPrototypes()
	{
		TabletPrototypes = new Dictionary<string, TreeTablet>();

		CreateTablet("Oak", 		TreeType.Strong, 	15, 	0, 		2,		.75f,	
					"",
					(T) => {});

		CreateTablet("Spruce", 		TreeType.Weak, 		10, 	3, 		0,		.30f, 	
					"-1" + Symbols.Production,
					(T) => {T.Data.Production -= 1;});

		CreateTablet("Maple", 		TreeType.Strong, 	9, 		1, 		0.5f,	.40f, 	
					"",
					(T) => {});

		CreateTablet("Nuts", 		TreeType.Bush,	 	3,	 	0,  	0,		.10f, 	
					"+0.5" + Symbols.Creation + " for " + Symbols.StrongTree,
					(T) => {if(T.Data.Type == TreeType.Strong)T.Data.Creation += 0.5f;});

		CreateTablet("Pine", 		TreeType.Weak, 		8, 		2, 		0,		.15f, 	
					"-0.5" + Symbols.Production,
					(T) => {T.Data.Production -= 0.5f;});

		CreateTablet("Raspberry", 	TreeType.Bush, 		3, 		1, 		0,		.07f,	
					"-0.25" + Symbols.Creation,
					(T) => {T.Data.Creation -= 0.25f;});

		CreateTablet("Basswood", 	TreeType.Bush, 		9, 		0,	 	0,		.15f,	
					"+1" + Symbols.Creation + " for " + Symbols.WeakTree,
					(T) => {if(T.Data.Type == TreeType.Weak)T.Data.Creation += 1;});

		CreateTablet("Mahogany", 	TreeType.Strong,	18,	 	1,  	2,		.80f,	
					"",
					(T) => {});

		CreateTablet("Willow", 		TreeType.Strong, 	13, 	1, 		1,		.70f,	
					"",
					(T) => {});

		CreateTablet("Bamboo", 		TreeType.Bush, 		20, 	1, 		1,		.25f,	
					"+1.5" + Symbols.Production + " for " + Symbols.Bush,
					(T) => {if(T.Data.Type == TreeType.Bush)T.Data.Production += 1.5f;});

	}	

	static void CreateTablet(string Name, TreeType Type, float Cost, float Production, float Creation, float LightObstruction, string AbilityDescription, Action<ForestTree> Ability)
	{
		tree_data Tree = new tree_data();
		Tree.Name = Name;
		Tree.TreeSprite = Sprites[Name];
		Tree.Type = Type;
		Tree.Cost = Cost;
		Tree.Production = Production;
		Tree.Creation = Creation;
		Tree.LightObstruction = LightObstruction;
		Tree.AbilityDescription = AbilityDescription;
		Tree.Ability = Ability;

		GameObject TabletGO = (GameObject)Instantiate(TabletPrefab);
		TabletGO.transform.parent = Camera.main.transform;
		TabletGO.transform.position = new Vector3(0, -1.5f, TabletGO.transform.position.z);
		TabletGO.name = Name + "Tablet";
		
		TreeTablet Tablet = TabletGO.GetComponent<TreeTablet>();
		Tablet.Tree = Tree;
		Tablet.Prototype = true;

		TabletGO.SetActive(false);

		TabletPrototypes.Add(Name, Tablet);
	}

	public static void MergeTablet(TreeTablet Tablet)
	{
		if(Tablet.Prototype)
		{
			return;
		}

		TabletPrototypes[Tablet.Tree.Name].Count++;
		Destroy(Tablet.gameObject);
	}

	public static TreeTablet SpawnTablet(string Name)
	{
		GameObject TabletGO = (GameObject) Instantiate(TabletPrototypes[Name].gameObject);
		TabletGO.SetActive(true);
		TreeTablet Tablet = TabletGO.GetComponent<TreeTablet>();
		Tablet.Prototype = false;

		return Tablet;
	}

	public static TreeTablet SpawnTablet(string Name, Vector2 Pos)
	{
		TreeTablet Tablet = SpawnTablet(Name);
		Tablet.transform.position = new Vector3(Pos.x, Pos.y, Tablet.transform.position.z);

		return Tablet;
	}

	public static TreeTablet SpawnTablet(string Name, Vector3 Pos)
	{
		TreeTablet Tablet = SpawnTablet(Name);
		Tablet.transform.position = Pos;
		
		return Tablet;
	}

	public static TreeTablet SpawnRandomTablet(Vector2 Pos)
	{
		return SpawnTablet(TabletPrototypes.Keys.ElementAt(UnityEngine.Random.Range(0, TabletPrototypes.Count)), Pos);
	}

    public static void PlaySoundEffect(AudioClip clip)
    {
        Camera.main.GetComponent<AudioSource>().PlayOneShot(clip, 0.5f);
    }

    public static void FindFurthestTree(ForestTree Ignore)
    {
    	ForestTree[] Trees = GameObject.FindObjectsOfType<ForestTree>();
    	ForestTree F = null;
    	foreach(ForestTree Tree in Trees)
    	{
    		if(Tree != Ignore)
    		{
    			if(F == null || F.transform.position.x > Tree.transform.position.x)
    			{
    				F = Tree;
    			}
    		}
    	}

    	FurthestTree = F;
    }
}
