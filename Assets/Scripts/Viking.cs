﻿using UnityEngine;
using System.Collections;

public class Viking : MonoBehaviour
{
    public GameObject rockPrefab;
    public bool archer = false;
    GameObject toBurn;
    int health = 2;
    bool burning = false;

    float offset;

    void Start()
    {
        offset = Random.Range(-0.3f, 0.5f);
    }

    float shootTimer = 0, shootMaxTime = 2;

    void Update()
    {
        if (!burning)
        {
            transform.position += Vector3.right * Time.deltaTime;

            if (archer)
            {
                if (Physics2D.OverlapCircle(transform.position, 7f + offset, 1 << LayerMask.NameToLayer("Squirrel")))
                {
                    shootTimer += Time.deltaTime;

                    if (shootTimer > shootMaxTime)
                    {
                        GameObject rock = Instantiate(rockPrefab, new Vector2(transform.position.x, 0.5f), Quaternion.identity) as GameObject;
                        GameData.PlaySoundEffect(Camera.main.GetComponent<GameData>().acornThrowAudioClip);
                        rock.GetComponent<Rigidbody2D>().AddForce(((Vector2.right) + Vector2.up) * Random.Range(4.0f, 10.0f), ForceMode2D.Impulse);
                        shootTimer = 0;
                    }
                }

            }
            foreach (Collider2D T in Physics2D.OverlapCircleAll(transform.position, 1f + offset, 1 << LayerMask.NameToLayer("Tree")))
            {
                if (T.gameObject.tag == "Tree")
                {
                    toBurn = T.gameObject;
                    burning = true;
                    return;
                }
            }
        }
        else
        {
            if (toBurn != null)
            {
                if (toBurn.GetComponent<ForestTree>().Burn())
                {
                    burning = false;
                    burnPlaying = false;
                }
                if (!burnPlaying)
                {
                    GameData.PlaySoundEffect(Camera.main.GetComponent<GameData>().fire);
                    burnPlaying = true;
                }
            }
            else
            {
                burning = false;
                burnPlaying = false;
            }
        }
    }

    bool burnPlaying = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Acorn")
        {
            Destroy(other.gameObject);
            health--;
            if (health <= 0)
            {
                //GameData.PlaySoundEffect(Camera.main.GetComponent<GameData>().vikingDie);
                Die();
            }
            else
            {
                GameData.PlaySoundEffect(Camera.main.GetComponent<GameData>().acornHitAudioClip);
            }
        }
    }

    void Die()
    {
        if (Random.Range(1, 4) == 2)
        {
            GameData.SpawnRandomTablet(new Vector2(transform.position.x, 1));
        }
        GameData.WaveVikings.Remove(this);
        Destroy(gameObject);
    }
}
