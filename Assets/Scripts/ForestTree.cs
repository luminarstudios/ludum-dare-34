﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class ForestTree : MonoBehaviour {

	public tree_data Data;
	public GameObject AOEArea;

	public List<ForestTree> AffectedTrees = new List<ForestTree>();

	tree_data BackupData;

	float ProductionTimer = 0;
	float CreationTimer = 0;

	float BurnTime = 5.0f;
	float BurnTimer = 0;

	bool Exist = false;

	void Start () {
		BackupData = new tree_data();
		BackupData.Name = Data.Name;
		BackupData.TreeSprite = Data.TreeSprite;
		BackupData.Type = Data.Type;
		BackupData.Cost = Data.Cost;
		BackupData.Production = Data.Production;
		BackupData.Creation = Data.Creation;
		BackupData.LightObstruction = Data.LightObstruction;
		BackupData.Ability = Data.Ability;

		gameObject.GetComponent<SpriteRenderer>().sprite = Data.TreeSprite;
		gameObject.GetComponent<SpriteRenderer>().color = new Color(0, 1, 0, 0.5f);
	}
	
	public bool Grow()
	{
		if(GameData.FurthestTree == null || transform.position.x < GameData.FurthestTree.transform.position.x)
		{
			GameData.FurthestTree = this;
		}

		Vector2 PointA = AOEArea.transform.position - AOEArea.transform.localScale*0.5f;
		Vector2 PointB = AOEArea.transform.position + AOEArea.transform.localScale*0.5f;

		Collider2D[] Colls = Physics2D.OverlapAreaAll(PointA, PointB);
		float LightLeft = 1;
		foreach(Collider2D Coll in Colls)
		{
			ForestTree T = Coll.gameObject.GetComponent<ForestTree>();
			if(T != null && T != this)
			{
				LightLeft -= (T.Data.LightObstruction * (1 - Mathf.Abs(T.transform.position.x - transform.position.x)/(AOEArea.transform.localScale*0.5f).x));
			}
		}

		if(LightLeft - Data.LightObstruction <= 0.1f)
		{
			return false;
		}

		foreach(Collider2D Coll in Colls)
		{
			ForestTree T = Coll.gameObject.GetComponent<ForestTree>();
			if(T != null && T != this)
			{
				if(!T.AffectedTrees.Contains(this))
				{
					T.Data.Ability(this);
					T.AffectedTrees.Add(this);
				}
				if(!AffectedTrees.Contains(T))
				{
					Data.Ability(T);
					AffectedTrees.Add(T);
				}
			}
		}

		gameObject.tag = "Tree";
		gameObject.GetComponent<SpriteRenderer>().color = Color.white;
		AOEArea.SetActive(false);
		Exist = true;

		GameData.StartVikingGame();
		return true;
	}

	public void RecalculateEffects(ForestTree Ignore)
	{
		Data = new tree_data();
		Data.Name = BackupData.Name;
		Data.TreeSprite = BackupData.TreeSprite;
		Data.Type = BackupData.Type;
		Data.Cost = BackupData.Cost;
		Data.Production = BackupData.Production;
		Data.Creation = BackupData.Creation;
		Data.LightObstruction = BackupData.LightObstruction;
		Data.Ability = BackupData.Ability;
		
		AffectedTrees.Remove(Ignore);
		Vector2 PointA = AOEArea.transform.position - AOEArea.transform.localScale*0.5f;
		Vector2 PointB = AOEArea.transform.position + AOEArea.transform.localScale*0.5f;
		foreach(Collider2D Coll in Physics2D.OverlapAreaAll(PointA, PointB))
		{
			ForestTree T = Coll.gameObject.GetComponent<ForestTree>();
			if(T != null && T != this && T != Ignore)
			{
				T.Data.Ability(this);
				if(!T.AffectedTrees.Contains(this))
				{
					T.AffectedTrees.Add(this);
				}
			}
		}
	}

	public bool Burn()
	{
		if(!Exist)
			return false;

		BurnTimer = 0;
		transform.GetChild(0).gameObject.SetActive(true);
		BurnTime -= Time.deltaTime;
		if(BurnTime <= 0)
		{
			Destroy(gameObject, 0.001f);
			return true;
		}
		else
		{
			return false;
		}
	}

	void Update () {
		if(!Exist)
			return;

		if(Data.Production > 0)
		{
			ProductionTimer += Time.deltaTime;
			if(ProductionTimer > 60 / Data.Production)
			{
				ProductionTimer = 0;
				GameData.saplings++;
			}
		}

		if(Data.Creation > 0)
		{
			CreationTimer += Time.deltaTime;
			if(CreationTimer > 60 / Data.Creation)
			{
				CreationTimer = 0;
				Instantiate(GameData.SquirrelPrefab, transform.position -Vector3.forward*0.001f, Quaternion.identity);
			}
		}
	}

	void FixedUpdate()
	{
		if(!Exist)
			return;

		BurnTimer += Time.deltaTime;
		if(BurnTimer > 0.1f)
		{
			transform.GetChild(0).gameObject.SetActive(false);
		}
	}

	void OnDestroy()
	{
		if(GameData.FurthestTree == this)
		{
			GameData.FindFurthestTree(this);
		}

		Vector2 PointA = AOEArea.transform.position - AOEArea.transform.localScale*0.5f;
		Vector2 PointB = AOEArea.transform.position + AOEArea.transform.localScale*0.5f;
		foreach(Collider2D Coll in Physics2D.OverlapAreaAll(PointA, PointB))
		{
			ForestTree T = Coll.gameObject.GetComponent<ForestTree>();
			if(T != null && T != this)
			{
				T.RecalculateEffects(this);
			}
		}

		if(Exist)
			GameData.CheckLoseGame();
	}
}
