﻿using UnityEngine;
using System.Collections;

public enum SquirrelStates { Idle, Walking, Attacking };

public class Squirrel : MonoBehaviour
{
    public GameObject acornPrefab;
    public float waypointX;
    public SquirrelStates state = SquirrelStates.Idle;

    float noticeRadius, offset;

    void Start()
    {
        offset = Random.Range(-0.3f, 0.5f);
        noticeRadius = Random.Range(4.6f, 5.4f);
    }

    float relocTimer = 0, relocMaxTime = 3;

    void Update()
    {
        switch (state)
        {
            case SquirrelStates.Idle:
                if (!Mathf.Approximately(transform.position.x, waypointX + offset))
                {
                    state = SquirrelStates.Walking;
                    break;
                }

                if (EnemySpot())
                {
                    state = SquirrelStates.Attacking;
                    break;
                }

                relocTimer += Time.deltaTime;
                if (relocTimer > relocMaxTime)
                {
                    if (Random.Range(1, 5) == 3)
                    {
                        relocTimer = 0;
                        waypointX = GameData.FurthestTree.transform.position.x + Random.Range(-6, 2);
                        relocMaxTime += Random.Range(-0.5f, 2f);
                    }
                    else
                    {
                        relocTimer = 0;
                        waypointX = transform.position.x + Random.Range(-6, 6);
                        relocMaxTime += Random.Range(-0.5f, 2f);
                    }
                }

                break;
            case SquirrelStates.Walking:
                if (Move())
                {
                    state = SquirrelStates.Idle;
                    transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
                    break;
                }
                else
                {
                    if (EnemySpot())
                    {
                        state = SquirrelStates.Attacking;
                        break;
                    }
                }
                break;
            case SquirrelStates.Attacking:
                if (EnemySpot())
                {
                    Shoot(-1);
                }
                else
                {
                    state = SquirrelStates.Idle;
                    break;
                }
                break;
        }
    }

    bool Move()
    {
        float X = waypointX + offset;
        if (transform.position.x == X)
        {
            return true;
        }
        if (X > transform.position.x)
        {
            transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
        }
        else
        {
            transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
        }
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(X, 0), Time.deltaTime);
        return false;
    }

    bool EnemySpot()
    {
        if (Physics2D.OverlapCircle(transform.position, noticeRadius, 1 << LayerMask.NameToLayer("Viking") | 1 << LayerMask.NameToLayer("Town")))
        {
            return true;
        }
        return false;
    }

    float shootTimer = 0, shootMaxTime = 1;
    void Shoot(int X)
    {
        shootTimer += Time.deltaTime;
        if (shootTimer > shootMaxTime)
        {
            shootTimer = 0;
            GameObject acorn = Instantiate(acornPrefab, new Vector2(transform.position.x, 0.5f), Quaternion.identity) as GameObject;
            GameData.PlaySoundEffect(Camera.main.GetComponent<GameData>().acornThrowAudioClip);
            acorn.GetComponent<Rigidbody2D>().AddForce(((Vector2.right * X) + Vector2.up) * Random.Range(2.0f, 7.0f), ForceMode2D.Impulse);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Viking")
        {
            Die();
        }

        if (other.gameObject.tag == "Rock")
        {
            Destroy(other.gameObject);
            Die();
        }
    }

    void Die()
    {
        GameData.PlaySoundEffect(Camera.main.GetComponent<GameData>().acornHitAudioClip);
        Destroy(gameObject);
    }

    void OnDestroy()
    {
        GameData.CheckLoseGame();
    }
}
