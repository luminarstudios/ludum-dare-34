﻿using UnityEngine;
using System.Collections;

public class Symbols {

	public static string Cost = "<quad material=1/>";

	public static string Production = "<quad material=2/>";

	public static string Creation = "<quad material=3/>";

	public static string StrongTree = "<quad material=4/>";

	public static string WeakTree = "<quad material=5/>";

	public static string Bush = "<quad material=6/>";
}
