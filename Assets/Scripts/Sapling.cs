﻿using UnityEngine;
using System.Collections;

public class Sapling : MonoBehaviour
{
    Vector3 pos;

    void Start()
    {
        pos = GameObject.FindGameObjectWithTag("Warehouse").transform.position;
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * 5);

        if ((transform.position - pos).sqrMagnitude < 0.1f)
        {
            GameData.saplings++;
            Destroy(gameObject);
        }
    }
}
