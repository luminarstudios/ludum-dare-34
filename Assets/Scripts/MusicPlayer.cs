﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour
{
    public AudioClip[] music;
    AudioSource source;
    int iterator = 0;

    void Start()
    {
        source = GetComponent<AudioSource>();

        ChangeSong();
    }

    void Update()
    {
        if (!source.isPlaying)
        {
            ChangeSong();
        }
    }

    void ChangeSong()
    {
        source.clip = music[iterator];
        source.Play();
        iterator++;
        if (iterator >= music.Length)
        {
            iterator = 0;
        }
    }
}
