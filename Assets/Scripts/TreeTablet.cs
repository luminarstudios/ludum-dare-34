﻿using UnityEngine;
using System;
using System.Collections;

public class TreeTablet : MonoBehaviour {

	public TextMesh CountTextMesh;
	public TextMesh DetailsTextMesh;
	public TextMesh SpecialTextMesh;
	public TextMesh TypeTextMesh;
	public SpriteRenderer TreeSpriteRend;

	int _count = 0;
	public int Count
	{
		get 
		{
			return _count;
		}
		set
		{
			_count = value;
			if(Count > 0)
			{
				Show();
			}
			if(Count <= 0)
			{
				_count = 0;
				Hide();
			}

			GameData.SortTabletsInHand();

			UpdateName();
		}
	}

	[HideInInspector]
	public tree_data Tree;

	bool _prototype = true;
	public bool Prototype
	{
		get
		{
			return _prototype;
		}
		set
		{
			_prototype = value;

			gameObject.GetComponent<Rigidbody2D>().isKinematic = _prototype;
			if(Prototype)
			{
				gameObject.transform.localScale = Vector3.one*4f;
			}
			else
			{
				gameObject.transform.localScale = Vector3.one;
			}
		}
	}

	void Start () {
		TreeSpriteRend.sprite = Tree.TreeSprite;

		DetailsTextMesh.text = 	Tree.Cost + Symbols.Cost + "\n" +
								Tree.Production + Symbols.Production + "\n" +
								Tree.Creation + Symbols.Creation + "<quad size=1 material=7/>";
		SpecialTextMesh.text = Tree.AbilityDescription + "<quad size=1 material=7/>";

		switch(Tree.Type)
		{
			case TreeType.Strong:
			{
				TypeTextMesh.text = Symbols.StrongTree;
			} break;
			case TreeType.Weak:
			{
				TypeTextMesh.text = Symbols.WeakTree;
			} break;
			case TreeType.Bush:
			{
				TypeTextMesh.text = Symbols.Bush;
			} break;
		}
		TypeTextMesh.text += "<quad size=1 material=7/>";

		UpdateName();
	}
	
	void Update () {

	}

	void UpdateName()
	{
		string Text = "";
		if(Prototype && Count > 1)
		{
			Text = Count + "";
			CountTextMesh.gameObject.transform.parent.gameObject.SetActive(true);
		}
		else
		{
			CountTextMesh.gameObject.transform.parent.gameObject.SetActive(false);
		}
		CountTextMesh.text = Text;
	}

	public void Show()
	{
		gameObject.SetActive(true);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	public void Use()
	{
		Deselect();
		GameData.saplings -= (int)Tree.Cost;
		--Count;
	}

	public void Select()
	{
		if(GameData.saplings < Tree.Cost)
		{
			return;
		}

		transform.localPosition += Vector3.up*0.5f;
		CameraInteraction.SelectedTablet = this;
		CameraInteraction.SelectedTree = SpawnTree();
		CameraInteraction.SelectedTree.gameObject.SetActive(false);
	}

	public void Deselect()
	{
		transform.localPosition -= Vector3.up*0.5f;
		CameraInteraction.SelectedTablet = null;
		CameraInteraction.SelectedTree = null;
	}

	ForestTree SpawnTree()
	{
		GameObject TreeGO = (GameObject)Instantiate(GameData.TreePrefab);
		TreeGO.name = Tree.Name;
		ForestTree T = TreeGO.GetComponent<ForestTree>();
		T.Data = Tree;

		return T;
	}
}
