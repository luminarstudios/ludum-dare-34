﻿using UnityEngine;
using System.Collections;

public class CameraInteraction : MonoBehaviour {

	public static TreeTablet SelectedTablet;
	public static ForestTree SelectedTree;

	public GameObject LeftBound, RightBound;

	Vector3 LastMousePos;
	Vector3 CurrentMousePos;

	float CameraWidth = 1;

	void Start () {
		CameraWidth = (Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height/2)) - Camera.main.transform.position).x * 2;
	}
	
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
		if(Input.GetKeyDown(KeyCode.R))
		{
			GameData.ReloadGame();
		}

		if(GameData.GameEnded)
		{
			return;
		}

		CurrentMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		if(SelectedTree != null && SelectedTablet != null)
		{
			if(GameData.FurthestTree != null)
			{
				Vector3 Pos = LeftBound.transform.position;
				Pos.x = GameData.FurthestTree.transform.position.x - CameraWidth/2;
				LeftBound.transform.position = Pos;
			}
			else
			{
				Vector3 Pos = LeftBound.transform.position;
				Pos.x = -CameraWidth/2;
				LeftBound.transform.position = Pos;
			}

			{
				Vector3 Pos = RightBound.transform.position;
				Pos.x = GameData.MaxX;
				RightBound.transform.position = Pos;
			}

			LeftBound.SetActive(true);
			RightBound.SetActive(true);
		}
		else
		{
			LeftBound.SetActive(false);
			RightBound.SetActive(false);
		}

		if(Input.GetMouseButtonDown(0))
		{
			bool NoHit = true;
			Collider2D[] Colls = Physics2D.OverlapPointAll((Vector2)CurrentMousePos);
			foreach(Collider2D Col in Colls)
			{
				NoHit = false;
				GameObject GO = Col.gameObject;

				TreeTablet Tablet = GO.GetComponent<TreeTablet>();
				if(Tablet != null)
				{
					if(SelectedTablet != null)
					{
						Destroy(SelectedTree.gameObject);
						SelectedTablet.Deselect();
					}
					if(SelectedTree != null)
					{
						Destroy(SelectedTree.gameObject);
						SelectedTree = null;
					}

					if(!Tablet.Prototype)
					{
						GameData.MergeTablet(Tablet);
					}
					else
					{
						Tablet.Select();
					}

					break;
				}

				NoHit = true;
			}

			if(NoHit)
			{
				if(SelectedTree != null)
				{
					if(SelectedTree.gameObject.activeInHierarchy)
					{
						if(SelectedTree.Grow())
						{
							SelectedTablet.Use();
						}
					}
					else
					{
						SelectedTablet.Deselect();
					}
				}
			}
		}
		else
		{
			if(SelectedTree != null && SelectedTablet != null)
			{
				if(Input.GetMouseButtonDown(1))
				{
					Destroy(SelectedTree.gameObject);
					SelectedTablet.Deselect();
				}
				else
				{
					Vector3 Pos = CurrentMousePos;
					Pos.z = SelectedTree.transform.position.z;
					if(Pos.x < GameData.MinX || Pos.x > GameData.MaxX)
					{
						SelectedTree.gameObject.SetActive(false);
					}
					else
					{
						bool OK = false;
						if(GameData.FurthestTree != null)
						{
							if(Pos.x + CameraWidth/2 < GameData.FurthestTree.transform.position.x)
							{
								OK = false;
							}
							else
							{
								OK = true;
							}
						}
						else
						{
							if(Pos.x > -CameraWidth/2)
							{
								OK = true;
							}
						}

						if(OK)
						{
							SelectedTree.gameObject.SetActive(true);
							if(Pos.y > -0.5f)
							{
								SelectedTree.gameObject.SetActive(true);
								Pos.x = CurrentMousePos.x;
								Pos.y = 0;
								SelectedTree.transform.position = Pos;
							}
							else
							{
								SelectedTree.gameObject.SetActive(false);
							}
						}
						else
						{
							SelectedTree.gameObject.SetActive(false);
						}
					}
				}
			}
		}

		if(Input.GetMouseButton(2))
		{
			Vector3 NewPos = Camera.main.transform.position + new Vector3((LastMousePos - CurrentMousePos).x, 0);

			NewPos.x = Mathf.Clamp(NewPos.x, GameData.MinX + CameraWidth/2, GameData.MaxX - CameraWidth/2);

			Camera.main.transform.position = NewPos;
			
		}

		LastMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	}
}

