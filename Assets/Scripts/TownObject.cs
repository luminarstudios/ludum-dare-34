﻿using UnityEngine;
using System.Collections;

public class TownObject : MonoBehaviour
{
    public int health = 5;

    void Start()
    {
        GameData.townObjects++;
    }

    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Acorn")
        {
            health--;
            GameData.PlaySoundEffect(Camera.main.GetComponent<GameData>().townHit);
            if (health <= 0)
            {
                GameData.townObjects--;

                if (GameData.townObjects <= 0)
                {
                    Destroy(transform.root.gameObject);
                    GameData.WinGame();
                }

                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }
    }
}
