﻿using UnityEngine;
using System.Collections;

public enum VikingTownStates { Charging, Wave, Wait };

public class VikingTown : MonoBehaviour
{
    VikingTownStates state = VikingTownStates.Wait;

    public GameObject vikingPrefab;

    void Start()
    {
        GameData.townObjects = 0;
    }

    float waveTime = 30;
    float waveTimer = 0;

    void Update()
    {
        switch (state)
        {
            case VikingTownStates.Wait:
                break;
            case VikingTownStates.Charging:
                waveTimer += Time.deltaTime;

                if (waveTimer > waveTime)
                {
                    state = VikingTownStates.Wave;
                    break;
                }
                break;
            case VikingTownStates.Wave:
                Wave();
                break;
        }
    }

    public void StartWaving()
    {
        state = VikingTownStates.Charging;
    }

    int vikingsToInstantiate = 1, vikingsToInstantiateHolder = 1;
    float timeTillViking = 1, timerTillViking = 1;

    void Wave()
    {
        if (vikingsToInstantiate > 0)
        {
            timerTillViking -= Time.deltaTime;

            if (timerTillViking < 0)
            {
                GameData.WaveVikings.Add((Instantiate(vikingPrefab, transform.position, Quaternion.identity) as GameObject).GetComponent<Viking>());

                vikingsToInstantiate--;

                timerTillViking = timeTillViking + Random.Range(-0.1f, 4f);
            }
        }
        else
        {
            if (GameData.WaveVikings.Count == 0)
            {
                NextWave();
                state = VikingTownStates.Charging;
            }
        }
    }

    void NextWave()
    {
        vikingsToInstantiateHolder += 2;
        vikingsToInstantiate = vikingsToInstantiateHolder;
        waveTime += 10 + Random.Range(1, 2);
    }
}
