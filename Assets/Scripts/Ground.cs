﻿using UnityEngine;
using System.Collections;

public class Ground : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Acorn" || other.gameObject.tag == "Rock")
        {
            GameData.PlaySoundEffect(Camera.main.GetComponent<GameData>().acornHitAudioClip);
            Destroy(other.gameObject);
        }
    }
}
